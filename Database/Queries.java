package Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import logging.ServerLogger;
import Player.Character;
import Configuration.ConfigurationManager;

public class Queries {
	private static ServerLogger log = ServerLogger.getInstance();
	
	public static PreparedStatement auth(Connection sqlc, String username, String hash) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SELECT * FROM accounts WHERE username = ? AND password = ?;");
		st.setString(1, username);
		st.setString(2, hash);		
		return st;
	}
	
	public static PreparedStatement newCharacter(Connection sqlc, String name, byte[] stats, int chClass, byte statpoints, int xCoords, int yCoords, int owner) {
		PreparedStatement st = null;
		try {
			st = sqlc.prepareStatement("INSERT INTO " +
					"characters(charname, charClass, faction, level, maxHP, currentHP, maxMana, currentMana, maxStamina, currentStamina, attack, defence, fame, flags, locationX, locationY, map, intelligence, vitality, agility, strength, dexterity, statpoints, skillpoints, ownerID) " +
					"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
	
		st.setString(1, name);  //character name
		st.setInt(2, chClass);  //class
		st.setInt(3, 0);	    //faction
		st.setInt(4, 1);	    //level
		st.setInt(5, 10);	    //max HP
		st.setInt(6, 10);		//current HP	 	
		st.setInt(7, 10);		//max mana
		st.setInt(8, 10);		//current mana
		st.setInt(9, 10);		//max stamina
		st.setInt(10, 10);		//current stamina
		st.setInt(11, 10);		//attack
		st.setInt(12, 10);		//defence
		st.setInt(13, 0);		//fame
		st.setInt(14, 10);		//flags
		
	
		
		st.setInt(15, xCoords);		//x coords
		st.setInt(16, yCoords);		//y coords
		
		st.setInt(17, 1);		//map
		
		st.setInt(18, (int)stats[0]);		//INT
		st.setInt(20, (int)stats[1]);		//AGI
		st.setInt(19, (int)stats[2]);		//VIT
		st.setInt(22, (int)stats[3]);		//DEX
		st.setInt(21, (int)stats[4]);		//STR
		
		st.setInt(23, (int)statpoints);		//statpoints
		st.setInt(24, 0);					//skillpoints
		st.setInt(25, owner); //owner account
		} catch (SQLException e) {
			log.logMessage(Level.WARNING, Queries.class, e.getMessage());
		}
		
		
		return st;
	}
	
	public static PreparedStatement getCharacterByName(Connection sqlc, String name) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SELECT * FROM characters WHERE charname = ?;");
		st.setString(1, name);
		return st;
	}
	
	public static PreparedStatement getCharacterByID(Connection sqlc, int id) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SELECT * FROM characters WHERE CharacterID = ?;");
		st.setInt(1, id);
		return st;
	}
	
	public static PreparedStatement getCharactersByOwnerID(Connection sqlc, int id) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SELECT * FROM characters WHERE ownerID = ?;");
		st.setInt(1, id);
		return st;
	}
	public static PreparedStatement showTables(Connection sqlc) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SHOW TABLES;");
		return st;
	}
	public static PreparedStatement showDatabases(Connection sqlc) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SHOW DATABASES;");
		return st;
	}
	public static PreparedStatement createAccountTable(Connection sqlc) throws Exception{
		PreparedStatement st =  sqlc.prepareStatement("CREATE TABLE `accounts` ("+
														"`accountID` int(10) unsigned NOT NULL AUTO_INCREMENT," +
														"`username` char(13) NOT NULL," +
														"`password` char(255) NOT NULL," +
														"`flags` int(11) DEFAULT NULL," +
														"PRIMARY KEY (`accountID`), " +
														"UNIQUE KEY `uniqueuser` (`username`) USING HASH " +
														") ENGINE=InnoDB DEFAULT CHARSET=ascii;");
		return st;
	}
	// some1's gotta write us parser for these -.-
	public static PreparedStatement createCharactersTable(Connection sqlc) throws Exception{
		PreparedStatement st =  sqlc.prepareStatement("CREATE TABLE `characters` (" +
							" `CharacterID` int(10) unsigned NOT NULL AUTO_INCREMENT,"+
							" `charname` char(16) NOT NULL," +
							" `charClass` int(11) NOT NULL, " +
							" `faction` int(11) DEFAULT NULL,"+
							" `level` int(11) NOT NULL DEFAULT '1',"+
							" `maxHP` int(11) NOT NULL, " +
							" `currentHP` int(11) NOT NULL, "+
							" `maxMana` int(11) NOT NULL," +
							" `currentMana` int(11) NOT NULL,"+
							" `maxStamina` int(11) NOT NULL, "+
							" `currentStamina` int(11) NOT NULL, "+
							" `attack` int(11) NOT NULL,"+
							" `defence` int(11) NOT NULL, "+
							" `fame` int(11) NOT NULL,"+
							" `flags` int(11) DEFAULT NULL,"+
							" `locationX` int(11) NOT NULL,"+
							" `locationY` int(11) NOT NULL,"+
							" `map` int(11) NOT NULL,"+
							" `intelligence` int(11) NOT NULL,"+
							" `vitality` int(11) NOT NULL,"+
							" `agility` int(11) NOT NULL,"+
							" `strength` int(11) NOT NULL,"+
							" `dexterity` int(11) NOT NULL,"+
							" `statpoints` int(11) NOT NULL,"+
							" `skillpoints` int(11) NOT NULL,"+
							" `ownerID` int(10) unsigned NOT NULL,"+
							"PRIMARY KEY (`CharacterID`),"+
							" UNIQUE KEY `uniquename` (`charname`) USING HASH,"+
							"KEY `owner` (`ownerID`),"+
							"CONSTRAINT `mapID` FOREIGN KEY (`map`) REFERENCES `maps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION," +
							"CONSTRAINT `owner` FOREIGN KEY (`ownerID`) REFERENCES `accounts` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE"+
							") ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=ascii;");
		return st;
	}
	public static PreparedStatement createMapTable(Connection con) throws Exception{
		PreparedStatement st = con.prepareStatement("CREATE TABLE `maps` ("+
													" `id` int(11) NOT NULL,"+
													"`name` varchar(45) NOT NULL,"+
													"`gridSize` int(11) NOT NULL,"+
													"`areaSize` int(11) NOT NULL,"+
													" `mapx` int(11) NOT NULL,"+
													"`mapy` int(11) NOT NULL,"+
													"`poolSize` int(11) NOT NULL,"+
													"PRIMARY KEY (`id`),"+
													"UNIQUE KEY `id_UNIQUE` (`id`)"+
													") ENGINE=InnoDB DEFAULT CHARSET=ascii;");
		return st;

	}
	public static PreparedStatement createMobDataTable(Connection con) throws Exception{
		PreparedStatement st = con.prepareStatement("CREATE TABLE `mobData` ("+
													"`mobID` int(11) NOT NULL,"+
													"`lvl` int(11) NOT NULL,"+
													"`attack` int(11) NOT NULL,"+
													"`defence` int(11) NOT NULL,"+
													"`maxhp` int(11) NOT NULL,"+
													"`basexp` int(11) NOT NULL,"+
													"`basefame` int(11) NOT NULL,"+
													"`aggroRange` int(11) NOT NULL,"+
													"`attackRange` int(11) NOT NULL,"+
													"`followRange` int(11) NOT NULL,"+
													"`moveRange` int(11) NOT NULL,"+
													"PRIMARY KEY (`mobID`),"+
													"UNIQUE KEY `mobID_UNIQUE` (`mobID`)"+
													") ENGINE=InnoDB DEFAULT CHARSET=ascii;");
		return st;
	}
	public static PreparedStatement createMobsTable(Connection con) throws Exception{
		PreparedStatement st = con.prepareStatement("CREATE TABLE `mobs` ("+
													"`id` int(11) NOT NULL,"+
													"`mobType` int(11) NOT NULL,"+
													"`map` int(11) NOT NULL,"+
													"`spawnCount` int(11) NOT NULL,"+
													"`spawnRadius` int(11) NOT NULL," +
													"`spawnX` int(11) NOT NULL,"+
													"`spawnY` int(11) NOT NULL,"+
													"`respawnTime` int(11) NOT NULL,"+
													"`waypointCount` int(11) NOT NULL,"+
													"`waypointHop` int(11) NOT NULL,"+
													"PRIMARY KEY (`id`),"+
													"UNIQUE KEY `id_UNIQUE` (`id`),"+
													"KEY `type` (`mobType`),"+
													"KEY `map` (`map`),"+
													"CONSTRAINT `map` FOREIGN KEY (`map`) REFERENCES `maps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,"+
													"CONSTRAINT `type` FOREIGN KEY (`mobType`) REFERENCES `mobData` (`mobID`) ON DELETE NO ACTION ON UPDATE NO ACTION" +
													") ENGINE=InnoDB DEFAULT CHARSET=ascii;");
		return st;
		
	}
	public static PreparedStatement addMap(Connection con, int id, String name, int gridsize, int areasize, int x, int y, int pool) throws Exception{
		PreparedStatement st = con.prepareStatement("INSERT INTO maps(id, name, gridSize, areaSize, mapx, mapy, poolSize) VALUES (?, ?, ?, ?, ?, ?, ?);");
		st.setInt(1, id);
		st.setString(2, name);
		st.setInt(3, gridsize);
		st.setInt(4, areasize);
		st.setInt(5, x);
		st.setInt(6, y);
		st.setInt(7, pool);
		return st;
	}
	public static PreparedStatement dropCharacterTable(Connection sqlc) throws Exception{
		if (ConfigurationManager.getProcessName().contentEquals("install")){
			PreparedStatement st =  sqlc.prepareStatement("DROP TABLE characters;");
			return st;
		}
		throw new Exception();
		//return null;
	}
	public static PreparedStatement dropAccountTable(Connection sqlc) throws Exception{
		if (ConfigurationManager.getProcessName().contentEquals("install")){
			PreparedStatement st =  sqlc.prepareStatement("DROP TABLE accounts;");
			return st;
		}
		throw new Exception();
	}

	public static PreparedStatement getMaps(Connection connection) throws Exception{
		PreparedStatement st = connection.prepareStatement("SELECT * FROM maps;");
		return st;
	}

	public static PreparedStatement dropMapTable(Connection connection) throws Exception {
		if (ConfigurationManager.getProcessName().contentEquals("install")){
			PreparedStatement st =  connection.prepareStatement("DROP TABLE maps;");
			return st;
		}
		throw new Exception();
	}

	public static PreparedStatement getMobData(Connection con, int mobID) throws Exception {
		PreparedStatement st = con.prepareStatement("SELECT * FROM mobData WHERE mobID = ?;");
		st.setInt(1, mobID);
		return st;
	}
	public static PreparedStatement getMobs(Connection con) throws Exception{
		PreparedStatement st = con.prepareStatement("SELECT * FROM mobs;");
		return st;
	}

	public static PreparedStatement getZones(Connection con) throws Exception {
		PreparedStatement st = con.prepareStatement("SELECT * FROM zones;");
		return st;
	}

	public static PreparedStatement dropZonesTable(Connection connection) throws Exception {
		if (ConfigurationManager.getProcessName().contentEquals("install")){
			PreparedStatement st =  connection.prepareStatement("DROP TABLE zones;");
			return st;
		}
		throw new Exception();
	}

	public static PreparedStatement dropMobsTable(Connection connection) throws Exception {
		if (ConfigurationManager.getProcessName().contentEquals("install")){
			PreparedStatement st =  connection.prepareStatement("DROP TABLE mobs;");
			return st;
		}
		throw new Exception();
	}

	public static PreparedStatement dropMobDataTable(Connection connection) throws Exception {
		if (ConfigurationManager.getProcessName().contentEquals("install")){
			PreparedStatement st =  connection.prepareStatement("DROP TABLE mobData;");
			return st;
		}
		throw new Exception();
	}
	public static PreparedStatement dropItemsTable(Connection connection) throws Exception {
		if (ConfigurationManager.getProcessName().contentEquals("install")){
			PreparedStatement st =  connection.prepareStatement("DROP TABLE items;");
			return st;
		}
		throw new Exception();
	}

	public static PreparedStatement createZoneTable(Connection connection) throws Exception {
		PreparedStatement st = connection.prepareStatement("CREATE TABLE `zones` ("+
													"`id` int(11) NOT NULL AUTO_INCREMENT,"+
													"`map` int(11) NOT NULL,"+
													"`startx` int(11) NOT NULL,"+
													"`starty` int(11) NOT NULL,"+
													"`width` int(11) NOT NULL,"+
													"`length` int(11) NOT NULL,"+
													"`type` varchar(45) NOT NULL,"+
													"PRIMARY KEY (`id`),"+
													"UNIQUE KEY `id_UNIQUE` (`id`),"+
													"KEY `myMap` (`map`),"+
													"CONSTRAINT `myMap` FOREIGN KEY (`map`) REFERENCES `maps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE"+
													") ENGINE=InnoDB DEFAULT CHARSET=ascii;");
		return st;
	}

	public static PreparedStatement CreateUserAccount(Connection connection, int accountID, String username, String password, int flags) throws Exception {
		PreparedStatement st = connection.prepareStatement("INSERT INTO accounts(accountID, username, password, flags) VALUES(?, ?, ?, ?);");
		st.setInt(1, accountID);
		st.setString(2, username);
		st.setString(3, password);
		st.setInt(4, flags);
		return st;
	}
	public static PreparedStatement createMobDataEntry(Connection con, int id, int lvl, int deff, int atk, int hp, int baseExp, int baseFame, int aggro, int follow, int move, int attrange) throws Exception{
		PreparedStatement st = con.prepareStatement("INSERT INTO mobData(mobID, lvl, attack, defence, maxhp, basexp, basefame, aggroRange, attackRange, followRange, moveRange) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
		st.setInt(1, id);
		st.setInt(2, lvl);
		st.setInt(3, atk);
		st.setInt(4, deff);
		st.setInt(5, hp);
		st.setInt(6, baseExp);
		st.setInt(7, baseFame);
		st.setInt(8, aggro);
		st.setInt(9, attrange);
		st.setInt(10, follow);
		st.setInt(11, move);
		return st;
	}
	
	public static PreparedStatement saveCharacterData(Connection con, Character ch) throws Exception {
		PreparedStatement st = con.prepareStatement("INSERT INTO characters(locationX, locationY, map) VALUES(?, ?, ?);");
		st.setInt(1, (int)ch.getlastknownX());
		st.setInt(2, (int)ch.getlastknownY());
		st.setInt(3, ch.getCurrentMap());
		return st;
	}

	public static PreparedStatement showGrants(Connection connection) throws Exception {
		PreparedStatement st = connection.prepareStatement("SHOW GRANTS;");
		return st;
	}
	public static PreparedStatement getItem(Connection con, int id) throws Exception{
		PreparedStatement st = con.prepareStatement("SELECT * FROM items WHERE itemid = ?;");
		st.setInt(1, id);
		return st;
	}
	public static PreparedStatement createItemsTable(Connection con) throws Exception {
		PreparedStatement st = con.prepareStatement("CREATE TABLE `items` (" +
													"`itemid` int(10) unsigned NOT NULL," +
													"`name` varchar(45) DEFAULT NULL," +
													"`description` varchar(45) DEFAULT NULL,"+
													"`category` int(11) NOT NULL," +
													"`slot` int(11) NOT NULL," +
													"`width` int(11) NOT NULL," +
													"`height` int(11) NOT NULL," +
													"`consumable` tinyint(4) NOT NULL," +
													"`permanent` tinyint(4) NOT NULL," +
													"`enhancement` tinyint(4) NOT NULL," +
													"`faction` tinyint(4) NOT NULL," +
													"`min_lvl` smallint(6) NOT NULL," +
													"`max_lvl` smallint(6) NOT NULL," +
													"`monk_usable` tinyint(4) NOT NULL," +
													"`sin_usable` tinyint(4) NOT NULL," +
													"`war_usable` tinyint(4) NOT NULL," +
													"`mage_usable` tinyint(4) NOT NULL," +
													"`str_req` smallint(6) NOT NULL," +
													"`int_req` smallint(6) NOT NULL," +
													"`vit_req` smallint(6) NOT NULL," +
													"`agi_req` smallint(6) NOT NULL," +
													"`dex_req` smallint(6) NOT NULL," +
													"`str_attr` smallint(6) NOT NULL," +
													"`int_attr` smallint(6) NOT NULL," +
													"`vit_attr` smallint(6) NOT NULL," +
													"`agi_attr` smallint(6) NOT NULL," +
													"`dex_attr` smallint(6) NOT NULL," +
													"`hp_attr` smallint(6) NOT NULL," +
													"`mana_attr` smallint(6) NOT NULL," +
													"`stamina_attr` smallint(6) NOT NULL," +
													"`atk_attr` smallint(6) NOT NULL," +
													"`def_attr` smallint(6) NOT NULL," +
													" `atk_success` float NOT NULL, "+
													"`def_success` float NOT NULL," +
													"`crit_rate` float NOT NULL," +
													"`crit_dmg` smallint(6) NOT NULL," +
													"`against_type` tinyint(4) NOT NULL," +
													"`type_dmg` float NOT NULL," +
													"`set_hash` int(10) unsigned NOT NULL," +
													"`pieces` tinyint(4) NOT NULL," +
													"`str_bonus` tinyint(4) NOT NULL," +
													"`int_bonus` tinyint(4) NOT NULL," +
													"`vit_bonus` tinyint(4) NOT NULL," +
													"`agi_bonus` tinyint(4) NOT NULL," +
													"`dex_bonus` tinyint(4) NOT NULL," +
													"`hp_bonus` smallint(6) NOT NULL," +
													"`mana_bonus` smallint(6) NOT NULL," +
													"`stamina_bonus` smallint(6) NOT NULL," +
													"`atk_bonus` tinyint(3) unsigned NOT NULL," +
													"`def_bonus` tinyint(3) unsigned NOT NULL," +
													"`atk_success_bonus` float NOT NULL," +
													"`def_success_bonus` float NOT NULL," +
													"`crit_rate_bonus` float NOT NULL," +
													"`type_dmg_bonus` float NOT NULL," +
													"`pvp_damage_inc` float NOT NULL," +
													"`expire_time` int(10) unsigned DEFAULT NULL," +
													"`base_id` int(10) unsigned DEFAULT NULL," +
													"`move_speed` tinyint(4) NOT NULL," +
													"`npc_price` int(10) unsigned DEFAULT NULL," +
													"`atk_range` float DEFAULT NULL," +
													"PRIMARY KEY (`itemid`)," +
													"UNIQUE KEY `equid_UNIQUE` (`itemid`)" +
													") ENGINE=InnoDB DEFAULT CHARSET=ascii;");
		return st;
	}
	public static PreparedStatement createEquipmentTable(Connection con) throws Exception {
		PreparedStatement st = con.prepareStatement("CREATE TABLE `equipments` (" +
													"`cap` int(10) unsigned DEFAULT '0', "+ 
													"`necklace` int(10) unsigned DEFAULT '0'," +
													"`cape` int(10) unsigned DEFAULT '0'," +
													"`jacket` int(10) unsigned DEFAULT '0'," +
													"`pants` int(10) unsigned DEFAULT '0'," +
													"`armor` int(10) unsigned DEFAULT '0'," +
													"`bracelet` int(10) unsigned DEFAULT '0'," +
													"`weapon1` int(10) unsigned DEFAULT '0'," +
													"`weapon2` int(10) unsigned DEFAULT '0'," +
													"`ring1` int(10) unsigned DEFAULT '0'," +
													"`ring2` int(10) unsigned DEFAULT '0'," +
													"`shoes` int(10) unsigned DEFAULT '0'," +
													"`bird` int(10) unsigned DEFAULT '0'," +
													"`tablet` int(10) unsigned DEFAULT '0'," +
													"`famepad` int(10) unsigned DEFAULT '0'," +
													"`mount` int(10) unsigned DEFAULT '0'," +
													"`beed` int(10) unsigned DEFAULT '0'," +
													"`belongsTo` int(10) unsigned NOT NULL," +
													"PRIMARY KEY (`belongsTo`)," +
													"UNIQUE KEY `belongsTo_UNIQUE` (`belongsTo`)," +
													"KEY `belongsTo_idx` (`belongsTo`)," +
													"CONSTRAINT `ownerID` FOREIGN KEY (`belongsTo`) REFERENCES `characters` (`CharacterID`) ON DELETE CASCADE ON UPDATE CASCADE" +
													") ENGINE=InnoDB DEFAULT CHARSET=ascii;");
		return st;


	}
	public static PreparedStatement getEquipment(Connection con, int id) throws Exception{
		PreparedStatement st = con.prepareStatement("SELECT * FROM equipments WHERE belongsTo = ?;");
		st.setInt(1, id);
		return st;
	}
	public static PreparedStatement createEquips(Connection con, int id, List<Integer> eq) throws Exception{
		PreparedStatement st = con.prepareStatement("INSERT INTO equipments(cap, necklace, cape, jacket, pants,armor, bracelet, weapon1, weapon2, ring1, ring2," +
													"shoes,bird,tablet,famepad,mount,beed,belongsTo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
		st.setInt(1, eq.get(0));
		st.setInt(2, eq.get(1));
		st.setInt(3, eq.get(2));
		st.setInt(4, eq.get(3));
		st.setInt(5, eq.get(4));
		st.setInt(6, eq.get(5));
		st.setInt(7, eq.get(6));
		st.setInt(8, eq.get(7));
		st.setInt(9, eq.get(8));
		st.setInt(10, eq.get(9));
		st.setInt(11, eq.get(10));
		st.setInt(12, eq.get(11));
		st.setInt(13, eq.get(12));
		st.setInt(14, eq.get(13));
		st.setInt(15, eq.get(14));
		st.setInt(16, eq.get(15));
		st.setInt(17, eq.get(16));
		st.setInt(18, id);
		return st;
	}
	public static PreparedStatement storeEquipments(Connection con, int id, List<Integer> eq) throws Exception{
		PreparedStatement st = con.prepareStatement("UPDATE equipments SET cap = ?, necklace = ?, cape = ?, jacket = ?, pants = ?," +
													"armor = ?, bracelet = ?, weapon1 = ?,weapon2 = ?,ring1 = ?," +
													"ring2 = ?, shoes = ?,bird = ?, tablet = ?,famepad = ?, mount = ?," +
													"beed = ?  WHERE belongsTo = ?;");
		st.setInt(1, eq.get(0));
		st.setInt(2, eq.get(1));
		st.setInt(3, eq.get(2));
		st.setInt(4, eq.get(3));
		st.setInt(5, eq.get(4));
		st.setInt(6, eq.get(5));
		st.setInt(7, eq.get(6));
		st.setInt(8, eq.get(7));
		st.setInt(9, eq.get(8));
		st.setInt(10, eq.get(9));
		st.setInt(11, eq.get(10));
		st.setInt(12, eq.get(11));
		st.setInt(13, eq.get(12));
		st.setInt(14, eq.get(13));
		st.setInt(15, eq.get(14));
		st.setInt(16, eq.get(15));
		st.setInt(17, eq.get(16));
		st.setInt(18, id);
		return st;
	}

	public static PreparedStatement dropEquipmentTable(Connection connection) throws Exception {
		if (ConfigurationManager.getProcessName().contentEquals("install")){
			PreparedStatement st = connection.prepareStatement("DROP TABLE equipments;");
			return st;
		}
		throw new Exception();
	}
}
