package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import item.ConsumableItem;
import item.EquipableItem;
import item.EquipableSetItem;
import item.ItemFrame;

public class ItemDAO {
	private final Connection sqlConnection = new SQLconnection().getConnection(); 
	private static ItemDAO instance;
	
	private ItemDAO() {
		instance = this;
	}
	
	public static ItemDAO getInstance() {
		return (instance == null) ? new ItemDAO() : instance;
	}
	
	// retrieve item data from db and return it as base type of ItemFrame
	public ItemFrame getItem(int id) {
		if (id == 0) return null;
		ResultSet rs = fetchItem(id);
		ItemFrame it = null;
		try {
			if (rs.next()){
				if(rs.getInt("set_hash") > 0) {
					it = this.getSetItem(id);
				} else if(rs.getInt("slot") > 0) {
					it = this.getEquipableItem(id);
				} else {
					it = new ItemFrame(id);
					setBasicProperties(it, rs);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return it;
	}
	// instantiate EquipableItem object with proper attributes
	private EquipableItem getEquipableItem(int id){
		EquipableItem it = null;
		if (id == 0) return null;
		ResultSet rs = fetchItem(id);
		try {
			if (rs.next()){
				it = new EquipableItem(id);
				setBasicProperties(it, rs);
				setEquipableProperties(it, rs);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return it;
	}
	// instantiate EquipableSetItem object with proper attributes
	private EquipableSetItem getSetItem(int id){
		if (id == 0) return null;
		EquipableSetItem it = null;
		ResultSet rs = fetchItem(id);
		try {
			if (rs.next()){
				it = new EquipableSetItem(id);
				setBasicProperties(it, rs);
				setEquipableProperties(it, rs);
				SetSetProperties(it, rs);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return it;
	}
	@SuppressWarnings("unused")
	private ConsumableItem getConsumableItem(int id){
		ConsumableItem it = new ConsumableItem(id);
		ResultSet rs = fetchItem(id);
		setBasicProperties(it, rs);
		setConsumableProperties(it, rs);
		return it;
	}
	private void setConsumableProperties(ConsumableItem it, ResultSet rs) {
		// TODO consumables items properties to db
		it.setMaxStackSize(1000);
	}
	// set Set specific properties
	private void SetSetProperties(EquipableSetItem it, ResultSet rs) {
		try{
			it.setSetBonusCritSucc(rs.getFloat("crit_rate_bonus"));
			it.setSetBonusDef(rs.getInt("def_bonus"));
			it.setSetBonusDefSucc(rs.getFloat("def_success_bonus"));
			it.setSetBonusHp(rs.getInt("hp_bonus"));
			it.setSetBonusAtk(rs.getInt("atk_bonus"));
			it.setSetBonusMana(rs.getInt("mana_bonus"));
			it.setSetBonusStamina(rs.getInt("stamina_bonus"));
			it.setSetBonusTypeDmg(rs.getInt("type_dmg_bonus"));
			it.setSetPieces(rs.getInt("pieces"));
			//str, int, vit, agi, dex
			short [] stats = new short[5];
			stats[0] = rs.getShort("str_bonus");
			stats[1] = rs.getShort("int_bonus");
			stats[2] = rs.getShort("vit_bonus");
			stats[3] = rs.getShort("agi_bonus");
			stats[4] = rs.getShort("dex_bonus");
			it.setStats(stats);
		} catch (SQLException e){
			
		}
		
	}
	// set basic item properties
	private void setBasicProperties(ItemFrame it, ResultSet rs){
		try {
			it.setHeight(rs.getInt("height"));
			it.setWidth(rs.getInt("width"));
			it.setMaxLvl(rs.getInt("max_lvl"));
			it.setMinLvl(rs.getInt("min_lvl"));
			it.setNpcPrice(rs.getInt("npc_price"));
			it.setExpirationTime(rs.getInt("expire_time")*1000);
		} catch (SQLException e){
			e.printStackTrace();
			
		} catch (Exception e){
			e.printStackTrace();
			
		}
	}
	// set equipable attributes
	private void setEquipableProperties(EquipableItem it, ResultSet rs) {
		try{
			it.setAtk(rs.getInt("atk_attr"));
			it.setDef(rs.getInt("def_attr"));
			it.setAttSucc(rs.getFloat("atk_success"));
			it.setCritDmg(rs.getShort("crit_dmg"));
			it.setCritSucc(rs.getFloat("crit_rate"));
			it.setDefSucc(rs.getFloat("def_success"));
			it.setFaction(rs.getShort("faction"));
			it.setHp(rs.getInt("hp_attr"));
			it.setMana(rs.getInt("mana_attr"));
			it.setSetHash(rs.getInt("set_hash"));
			it.setStamina(rs.getInt("stamina_attr"));
			it.setType(rs.getInt("againt_type"));
			it.setTypeDmg(rs.getInt("type_dmg"));
		
			boolean[] usableClass = new boolean[4];
			usableClass[0] = rs.getInt("war_usable") == 1;
			usableClass[1] = rs.getInt("sin_usable") == 1;
			usableClass[2] = rs.getInt("mage_usable") == 1;
			usableClass[3] = rs.getInt("monk_usable") == 1;
			it.setUsableClass(usableClass);
		
			short [] statreg = new short[5];
			statreg[0] = rs.getShort("str_reg");
			statreg[1] = rs.getShort("int_reg");
			statreg[2] = rs.getShort("vit_reg");
			statreg[3] = rs.getShort("agi_reg");
			statreg[4] = rs.getShort("dex_reg");
			it.setStatRequirements(statreg);
		
			short []statbon = new short[5];
			statbon[0] = rs.getShort("str_attr");
			statbon[1] = rs.getShort("int_attr");
			statbon[2] = rs.getShort("vit_attr");
			statbon[3] = rs.getShort("agi_attr");
			statbon[4] = rs.getShort("dex_attr");
			it.setStatBonuses(statbon);
		} catch (SQLException e){
				
		} catch (Exception e){
				
		}
		
	}
	
	// return resultset to item in question
	private ResultSet fetchItem(int id) {
		ResultSet rs = null;
		try {
			rs = Queries.getItem(this.sqlConnection, id).executeQuery();
		} catch (SQLException e){
			e.printStackTrace();
			
		} catch (Exception e){
			e.printStackTrace();
		}
		return rs;
	}

}
