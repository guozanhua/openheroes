package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import logging.ServerLogger;

public class InstallDAO {
	private ServerLogger log = ServerLogger.getInstance();
	private static InstallDAO instance;
	
	
	private InstallDAO(){
		this.log = ServerLogger.getInstance();
	}
	public static InstallDAO getInstance(){
		if (instance == null){
			instance = new InstallDAO();
		}
		return instance;
	}
	
	public boolean isEmpty(){
		boolean b = true;
		try{
			Queries.showGrants(new SQLconnection(false).getConnection()).executeQuery();
			// b = rs.next();
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}
	public boolean tableExists(String table){
		boolean b = false;
		try{
			ResultSet rs = Queries.showTables(new SQLconnection(false).getConnection()).executeQuery();
			while( rs.next()){
				if (table.contentEquals(rs.getString(1))) b = true;
			}
			
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}
	public boolean dropTable(int n){
		boolean b = false;
		try{
			if (n == 0){
				b = Queries.dropAccountTable(new SQLconnection().getConnection()).execute();
			}
			else if (n == 1){
				b = Queries.dropCharacterTable(new SQLconnection().getConnection()).execute();
			}
			else if (n == 2){
				b = Queries.dropItemsTable(new SQLconnection().getConnection()).execute();
			}
			else if (n == 3){
				b = Queries.dropMapTable(new SQLconnection().getConnection()).execute();
			}
			else if (n == 4){
				b = Queries.dropMobsTable(new SQLconnection().getConnection()).execute();
			}
			else if (n == 5){
				b = Queries.dropMobDataTable(new SQLconnection().getConnection()).execute();
			}
			else if (n == 6){
				b = Queries.dropEquipmentTable(new SQLconnection().getConnection()).execute();
			}
			
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}
	public boolean createAccountTable() {
		boolean b = true;
		try{
			Queries.createAccountTable(new SQLconnection().getConnection()).execute();
			
		}catch (SQLException e) {
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());

			b = false;
		}
		return b;
		
	}
	public boolean createCharacterTable() {
		boolean b = true;
		try{
			Queries.createCharactersTable(new SQLconnection().getConnection()).execute();
			
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
		
	}
	public boolean createMapTable() {
		boolean b = true;
		try{
			Queries.createMapTable(new SQLconnection().getConnection()).execute();
			
		}catch (SQLException e) {
			log.severe(this, "Database error: " +e.getMessage());
			// e.printStackTrace();
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}
	public boolean createMobDataTable() {
		boolean b = true;
		try{
			Queries.createMobDataTable(new SQLconnection().getConnection()).execute();
			
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}
	public boolean createMobsTable() {
		boolean b = true;
		try{
			Queries.createMobsTable(new SQLconnection().getConnection()).execute();
			
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}
	public boolean createItemsTable() {
		boolean b = true;
		try{
			Queries.createItemsTable(new SQLconnection().getConnection()).execute();
			
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}
	public boolean addMap(int id, String name, int gridsize, int areasize, int x, int y, int pool) {
		boolean b = true;
		try{
			Queries.addMap(new SQLconnection().getConnection(), id, name, gridsize, areasize,x,y,pool).execute();
			
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}
	public boolean CreateAccount(int accountID, String username, String password, int flags) {
		boolean b = true;
		try{
			Queries.CreateUserAccount(new SQLconnection().getConnection(), accountID, username, password, flags).execute();
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}
	public boolean createMobDataEntry(Connection sql, int id, int lvl, int att, int deff, int hp, int basexp, int basefame, int aggro, int attrange, int follow,int move) {
		boolean b = true;
		try{
			Queries.createMobDataEntry(sql, id, lvl, deff, att, hp, basexp, basefame, aggro, follow, move, attrange).execute();
			
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}

		return b;
		
	}
	public boolean createEquipmentTable() {
		boolean b = true;
		try{
			Queries.createEquipmentTable(new SQLconnection().getConnection()).execute();
			
		}catch (SQLException e) {
			// e.printStackTrace();
			log.severe(this, "Database error: " +e.getMessage());
			b = false;
		}
		catch (Exception e) {
			// e.printStackTrace();
			log.severe(this, "Unspecified error:" +e.getMessage());
			b = false;
		}
		return b;
	}

}
