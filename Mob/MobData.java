package Mob;

/*
 * MobData.class
 * Stores all mobs data 
 */

public class MobData implements Cloneable{
	private int lvl, attack, defence, maxhp, basexp, basefame;
	private int aggroRange = 30;
	private int followRange = 200;
	private int moveRange = 500;
	private int attackRange = 5;
	private long respawnTime = 10000;
	private int [] grid;
	private int mobID;
	private int gridID;
	private int waypointCount, waypointHop;
	private int moveSpeed = 50;
	private int waypointDelay = 4;
	
	
	public int getLvl() {
		return lvl;
	}
	public void setLvl(int lvl) {
		this.lvl = lvl;
	}
	public int getAttack() {
		return attack;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}
	public int getDefence() {
		return defence;
	}
	public void setDefence(int defence) {
		this.defence = defence;
	}
	public int getBasexp() {
		return basexp;
	}
	public void setBasexp(int basexp) {
		this.basexp = basexp;
	}

	public int getAggroRange() {
		return aggroRange;
	}
	public void setAggroRange(int aggroRange) {
		this.aggroRange = aggroRange;
	}
	public int getFollowRange() {
		return followRange;
	}
	public void setFollowRange(int followRange) {
		this.followRange = followRange;
	}
	public int getMoveRange() {
		return moveRange;
	}
	public void setMoveRange(int moveRange) {
		this.moveRange = moveRange;
	}
	public long getRespawnTime() {
		return respawnTime;
	}
	public void setRespawnTime(long respawnTime) {
		this.respawnTime = respawnTime;
	}
	public int[] getGrid() {
		return grid;
	}
	public void setGrid(int[] grid) {
		this.grid = grid;
	}
	public int getMobID() {
		return mobID;
	}
	public void setMobID(int mobID) {
		this.mobID = mobID;
	}
	public int getGridID() {
		return gridID;
	}
	public void setGridID(int gridID) {
		this.gridID = gridID;
	}
	public int getWaypointHop() {
		return waypointHop;
	}
	public void setWaypointHop(int waypointHop) {
		this.waypointHop = waypointHop;
	}
	public int getWaypointCount() {
		return waypointCount;
	}
	public void setWaypointCount(int waypointCount) {
		this.waypointCount = waypointCount;
	}
	public int getAttackRange() {
		return attackRange;
	}
	public void setAttackRange(int attackRange) {
		this.attackRange = attackRange;
	}
	public int getBasefame() {
		return basefame;
	}
	public void setBasefame(int basefame) {
		this.basefame = basefame;
	}
	public int getMaxhp() {
		return maxhp;
	}
	public void setMaxhp(int maxhp) {
		this.maxhp = maxhp;
	}
	public int getMoveSpeed() {
		return this.moveSpeed;
	}
	public int getWaypointDelay() {
		return waypointDelay;
	}
	public void setWaypointDelay(int waypointDelay) {
		this.waypointDelay = waypointDelay;
	}
	
}
